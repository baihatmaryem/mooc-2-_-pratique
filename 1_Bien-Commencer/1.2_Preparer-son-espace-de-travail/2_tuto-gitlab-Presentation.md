# Présentation de Git Lab #

Dans ce MOOC, vous serez amenés à créer des fiches et des ressources. Pour les partager (entre nous au sein du MOOC mais aussi avec vos collègues), nous vous proposons d’utiliser la plateforme [GitLab](https://fr.wikipedia.org/wiki/GitLab). C’est une plateforme basée sur [Git](https://fr.wikipedia.org/wiki/Git).

## Pourquoi GitLab ?

Nous vous invitons à mettre votre travail sur **gitlab.com** pour plusieurs raisons : c'est un **outil coopératif** général qui va nous permettre de travailler ensemble sur ces ressources avec lesquelles nous apprenons à enseigner, en gardant trace des versions, les partageant sur le [forum communautaire](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/191). Elles seront aussi directement disponibles pour les réutiliser en classe, voire en co-construire avec d'autres collègues; de plus les plateformes git sont des outils de base en ingénierie logicielle ou pour d'autres projets coopératifs numériques, alors ce sera une initiation qui vous resservira.

## Comment ça marche ?

Gitlab est une **"forge" de documents co-construits** : on y crée des répertoires et on y crée ou dépose des fichiers, puis au fil des modifications le système offre toutes les fonctionnalités utiles : historique des versions, possibilité de faire bifurquer _("fork")_ une partie des documents pour travailler sur une branche _("branch")_ avec des variantes différentes, et outils pour ensuite fusionner _("merger")_ au mieux; quand c'est du code informatique, on peut ainsi faire un suivi des bugs, et offrir une intégration et livraison continue du résultat; mais cela permet aussi de créer d'autres objets, par exemple : un ouvrage mis à jour en permanence.
