## Exemple de mise en oeuvre de séance

**Fiche élève cours :** https://pixees.fr/informatiquelycee/prem/c2c.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/prem/c2a.html

**Fiche prof :** ./fiche-prof-nsi-1ere-sequence-python.md

**Etape 0 :** Mise en place: 

- installation des élèves sur les machines, connection et lancement de python
- ouverture de la page web de la fiche élève cours et activité, 
- mise en place de la feuille de travail pour que l'élève garde la trace de ce qui a été fait `notebook ?`

**Etape 1 :** Activités 2.1 à 2.4

- L'élève copie-colle la portion de code et la complète puis garde la trace du résultat.

**Etape 2 :** Activités 2.5 à 2.11

- L'élève fait l'effort de ne _pas_ juste exécuter le programme mais essayer d'abord de prédire la réponse sur le papier avant

En cas d'erreur ... c'est super: on invite à comprendre quel raisonnement (pas absurde) a conduit à cette réponse qui ne correspond pas au résultat obtenu avec l'interpréteur, il est possible que le choix "sémantique" de l'élève corresponde à d'autres options, qui se rencontrent par exemple avec d'autres langages.

**Etape 3 :** Activités 2.12 à 2.15

Ces activités permettent d'aller plus loin, optionnelles, elles peuvent être réalisées dans l'ordre qui inspire le mieux.

Elles nécessitent toutes un petit travail de _conception_ sur papier avant de passer au codage pour na pas trop tâtonner

**Etape 4 :** 

Quelles autre activité pourrait-être proposée ? Oui, on change les rôles, et on invite ici à ce que l'élève soi-même propose une petite activité qui mettrait en oeuvre les notions abordées ici, par exemple les tuples.




