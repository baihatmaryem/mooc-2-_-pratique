## Exemple de mise en oeuvre de séance

**Fiche élève cours :** https://pixees.fr/informatiquelycee/term/c7c.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/term/c7a.html

**Fiche prof :** ./fiche-prof-nsi-term-arbres.md

**Etape 0 :** Mise en place: 

- Les élèves relisent le cours et le gardent affichés pour faire les exercices et y retrouver le vocabulaire

- On pourra aussi lire https://fr.wikipedia.org/wiki/Arbre_(informatique) et comparer le niveau de détail entre une encyclopédie et le cours d'informatique

**Etape 1 :** activité 7.1

- On encourage l'élève à trouver un ou des exemples dans la vie courante au delà du champ technique

**Etape 2 :** activité 7.2

- On regardera attentivement les réponses erronnées, c'est le plus interessant, car cela permet à l'élève de détecter là où quelque chose reste à comprendre ou à préciser

**Etape 3 :** activité 7.3 et 7.4

- On pourra là encore s'aider de https://fr.wikipedia.org/wiki/Arbre_de_recherche pour avoir un éclairage complémentaire du cours synthétique

- Les activités 7.2 à 7.4 pourraient au sein de la classe être "corrigé par les pairs" de la manière suivante:
  - Chaque élève fait l'activité en autonomie sans communiquer avec l'autre
  - Une fois achevée chaque paire d'élève échange les feuilles et propose éventuellement des corrections sans communiquer
  - Dans une troisième étape, si il y a des différences, les élèves voient entre elles ou eux qu'elle peut être la bonne réponse





